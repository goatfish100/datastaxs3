#!/bin/bash
 # Bash Menu Script Example 
 PS3='Please enter your choice: ' options=("Run App 1" "Test App 2" "Test App 3" "Get Post signed URL 4" "Check Resource 5" "Upload 6" "Download Resource 7" "Exit 8")
  select opt in "${options[@]}" 
    do case $opt in
     "Run App 1")
        echo "python3 mainapp.py"
        python3 mainapp.py
        break 
        ;;
      "Test App 2") 
        echo "start localstack"
        localstack start   
        break 
        ;; 
      "Test App 3") 
        echo "python3 -m pytest"
        python3 -m pytest
        break 
        ;; 
     "Get Post signed URL 4") 
        echo "curl localhost:5000/s3post/YOUR_OBJ"
        break 
        ;; 
     "Check Resource 5") 
        echo "curl localhost:5000/s3check/morebeermofo"
        break
        ;;
     "Upload 6") 
        echo "curl -T filename https://goatfish100.s3.amazonaws.com/signed_URL"
        break
        ;;
     "Download Resource 7") 
        echo "curl https://goatfish100.s3.amazonaws.com/signed_URL"
        break
        ;;        
     "Exit 8")
	     echo 'bye'
        break
        ;;        
    esac
done
