import pytest
import boto3
from mainapp import app as flask_app
from dotenv import load_dotenv, find_dotenv
import os
import logging
from logging.config import fileConfig
import localstack_client.session

load_dotenv(find_dotenv())
AWS_KEY = os.getenv('AWS_ACCESS_KEY')
AWS_SECRET = os.getenv('AWS_SECRET')
AWS_BUCKET = os.getenv('AWS_BUCKET')
LOGGER = logging.getLogger()
S3_RESOURCE = "s3_RESOURCE.TXT"

@pytest.fixture(autouse=True)
def boto3_localstack_patch(monkeypatch):
    session_ls = localstack_client.session.Session()
    monkeypatch.setattr(boto3, "client", session_ls.client)
    monkeypatch.setattr(boto3, "resource", session_ls.resource)
    s3_client = boto3.client('s3')
    s3_client.create_bucket(Bucket="JLS")
    s3_client.create_bucket(Bucket="goatfish100")
    s3_client.upload_file('testfile', 'goatfish100', 'testfile')

@pytest.fixture
def app():
    yield flask_app



@pytest.fixture
def client(app):
    LOGGER.info("@pytest.fixture")

    s3_client = boto3.client('s3')
    
    return app.test_client()
