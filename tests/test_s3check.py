import json
import pytest
# Boto3 library generates signed url locally - it has all key's
# it needs and does not contact AWS - thus no need to mock/dummy 
# service




def test_index(app, client):
    res = client.get('/s3check/testfile')
    assert res.status_code == 200
    assert(res.get_data(as_text=True))
    assert "True" in res.get_data(as_text=True)

    res = client.get('/s3check/nonexist')
    assert res.status_code == 200
    assert(res.get_data(as_text=True))
    assert "False" in res.get_data(as_text=True)
