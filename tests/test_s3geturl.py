import json

# Boto3 library generates signed url locally - it has all key's
# it needs and does not contact AWS - thus no need to mock/dummy 
# service
#A good resource
def test_good_index(app, client):
    res = client.get('/s3geturl/testfile')
    assert res.status_code == 200
    assert json.loads(res.get_data(as_text=True))

    assert "url" in res.get_data(as_text=True)

#A bad resource
def test_bad_index(app, client):
    res = client.get('/s3geturl/noresourcedoesnotexist')
    assert res.status_code == 200
    assert json.loads(res.get_data(as_text=True))

    #assert "True" in res.get_data(as_text=True)